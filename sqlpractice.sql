create database student;
use student;
create table stu(
id int not null auto_increment,
name varchar(20) NOT NULL,
age int not null,
primary key(id));
show tables;
desc stu;
alter table stu add gender varchar(20) not null after age;
desc stu;
insert into stu values( null,"kumar",21,"Manle");
select * from stu;
insert into stu(name, age,city) values('ram',21,'chennai');
select * from stu;
 insert into stu(name, age,gender) values('mounika',21,'female');
 select * from stu;
 update stu set age=20 where name="ram";
 select * from stu;
  update stu set age=20 where name="kumar";
  select * from stu;
  update stu set age=20 where id=2;
  select * from stu;
  delete from stu where id=2;
  select * from stu;
  alter table stu rename to student;
  show tables;
  alter table student rename to stu;
  truncate table stu;
  select * from stu; 
  insert into stu(name, age,gender) values('mounika',21,'female'),('kumar',21,'male'),('kuru',23,'male'),('mounika',25,'female'),('Ram',26,'male');
  select * from stu;
  show tables;
  insert into stu(name, age,gender) values('mounika',21,'female'),('kumar',21,'male'),('kuru',23,'male'),('mounika',25,'female'),('Ram',26,'male');
  select * from stu;
  select name, age from stu where age=21;
  select * from stu;
  select name , age from stu where gender='male' AND age=21;
  select name,gender, age from stu where age>25 OR name='mounika';
  
  use new;
create view emp1 As select emp.name,emp.age,branch.salary from emp  join branch  ON emp.id = branch.id order by salary;
select * from emp1;
select * from emp1 where name='kuru';

create OR replace view emp1 As select emp.name,emp.age,branch.salary from emp  join branch  ON emp.id = branch.id ;
select * from emp1;

drop view emp1;
select * from emp1;
show tables;
show table status;

select * from emp 
where salary=(select max(salary) from emp);

select * from employee 
where name=(select count(name) from employee);

select * from employee where name=
(select count(name) from employee);
select avg(name) from employee;

select branch.id, branch.salary from branch
where EXISTS (select * from employee
where name='kumar' AND branch.id = employee.id);

select * from branch;
select * from employee;
update employee set name='kamal' where id=1;
select * from employee; 
update employee set name='kamal' where age=21;

create table emp1( id int not null, name varchar(20) not null,age int not null,salary int not null );
insert into emp1(id, name,age ,salary) values(1,'kumar',21,20000),(2,'kuru',21,30000),
(3,'mounika',21,40000),(4,'Raja',22,20000);
select * from emp1;
update emp1 set name='kamal' where id=1;
select name, salary from emp1 where lower(name);
select * from emp1;
select name, salary from emp1 where lower(name)='kumar';
select name, salary from emp1 where concat('kumar','kamal');
select upper(name) from emp1;
select upper(name) from emp1 where name='kumar';
select concat(name,salary) from emp1 where salary=40000;
update emp1 set name='Ramesh' where salary=20000 ;
update emp1 set name='guna' where id=1;
select now();
select datediff(curdate(),'2001-02-21') As days;
select date_format(curdate(),"%M/%D/%Y");

select date_format(curdate(),"%m/%/%Y");
select * from emp1 intersect select * from employee;

select * from branch; 
select  count(id) total from employee where salary>20000
group by age having count(id)>0
order by age;

use new;
create table emp2(name varchar(20) not null,acc varchar(30) ,primary key(acc));
insert into emp2 values('kamal',1234);
select * from emp2;
insert into emp2 values('kamal',125);
create table salary(acc varchar(30),january varchar(30),primary key(acc),foreign key(acc)
references emp2(acc));
insert into salary values('1234',20000);
insert into salary values('125',50000);
select * from salary;
select * from emp2;
delete from emp2 where acc='1285';
commit;

  
