create database new;    
use new;
create table employee ( id int not null auto_increment primary key, name varchar(20) not null,age int not null,salary int not null );
insert into employee( name,age ,salary) values('kumar',21,20000),('kuru',21,30000),
('mounika',21,40000),('Raja',22,20000);
select * from employee;
select name , salary, age from employee where name = 'kumar' AND salary<30000;
select name , salary, age from employee where (name = 'kumar' OR salary<30000) AND age=21;
select * from employee;
update employee set name='kumar', age=22 ,salary=40000 where id=2;
select * from employee; 
select * from employee;
update employee set age=32 where id=2;
select * from employee;
select name from employee;
select name ,age,salary from employee where id=3;
select distinct salary, name from employee;
select count(name) from employee;
select max(name) from employee;
select name ,salary,10*salary+100"Anual salary" from employee;
select * from employee;
select name,salary,12*(salary+100) anualsalary from employee;
select count(anualsalary) from employee;
select * from employee;
select name ,salary from employee where salary between 20000 AND 40000;
select name,age, salary from employee  where age IN(27,22);
select name , salary from employee where name like'k%';
select name ,age from employee where name like'_a';
update employee set name='Aathi', age=33,salary=30000 where id=5;
select * from employee;
select name, salary from employee where name like'_a%';
select name, salary from employee where salary>20000 OR name like'%aa%';
select name ,salary from employee order by name;
select name ,salary from employee order by name desc;
select name ,salary from employee order by name ,salary desc;
select * from employee;
select * from employee name limit 0,1;
select * from employee order by name desc limit 0,1;  
select  name ,salary, count(id) from employee group by salary;
create table Attendtance(id int not null, date int not null,status varchar(2));
insert into Attendtance values(1,2/3/2000,'p');
select * from Attendtance ;
select * from employee;
select * from Attendtance where id=1;
select * from employee where id=1;

use new;
create table emp( id int not null auto_increment primary key, name varchar(20) not null,age int not null,salary int not null );
insert into emp( name,age ,salary) values('kumar',21,20000),('kuru',21,30000),
('mounika',21,40000),('Raja',22,20000);
drop table emp;
show tables;
select * from emp;
create table branch(id int not null auto_increment primary key, job_dec varchar(20) not null,salary int not null);
insert into branch(job_dec,salary) values ('managre',50000),('clerk',20000),('assistent',10000);
select * from emp;
select * FROM branch;
select emp.name,emp.age,branch.salary from emp , branch where emp.id = branch.id order by salary;
select emp.name, emp.age,branch.salary from emp,branch where emp.id = branch.id order by salary;
select emp.name,emp.age,branch.salary from emp cross join branch ;
select * from emp intersect select * from branch;
select * from emp;
select * from branch;
update branch set age=22 where id=1;
select * from branch;
select * from emp union select * from branch;
select * from emp union all select * from branch;

use new;
create view emp1 As select emp.name,emp.age,branch.salary from emp  join branch  ON emp.id = branch.id order by salary;
select * from emp1;
select * from emp1 where name='kuru';

create OR replace view emp1 As select emp.name,emp.age,branch.salary from emp  join branch  ON emp.id = branch.id ;
select * from emp1;

drop view emp1;
select * from emp1;
show tables;
show table status;

select * from emp 
where salary=(select max(salary) from emp);

select * from employee 
where name=(select count(name) from employee);

select * from employee where name=
(select count(name) from employee);
select avg(name) from employee;

select branch.id, branch.salary from branch
where EXISTS (select * from employee
where name='kumar' AND branch.id = employee.id);

select * from branch;
select * from employee;
update employee set name='kamal' where id=1;
select * from employee; 
update employee set name='kamal' where age=21;

create table emp1( id int not null, name varchar(20) not null,age int not null,salary int not null );
insert into emp1(id, name,age ,salary) values(1,'kumar',21,20000),(2,'kuru',21,30000),
(3,'mounika',21,40000),(4,'Raja',22,20000);
select * from emp1;
update emp1 set name='kamal' where id=1;
select name, salary from emp1 where lower(name);
select * from emp1;
select name, salary from emp1 where lower(name)='kumar';
select name, salary from emp1 where concat('kumar','kamal');
select upper(name) from emp1;
select upper(name) from emp1 where name='kumar';
select concat(name,salary) from emp1 where salary=40000;
update emp1 set name='Ramesh' where salary=20000 ;
update emp1 set name='guna' where id=1;
select now();
select datediff(curdate(),'2001-02-21') As days;
select date_format(curdate(),"%M/%D/%Y");

select date_format(curdate(),"%m/%/%Y");
select * from emp1 intersect select * from employee;

select * from branch; 
select  count(id) total from employee where salary>20000
group by age having count(id)>0
order by age;

use new;
create table emp2(name varchar(20) not null,acc varchar(30) ,primary key(acc));
insert into emp2 values('kamal',1234);
select * from emp2;
insert into emp2 values('kamal',125);
create table salary(acc varchar(30),january varchar(30),foreign key(acc)
references emp2(acc) ON DELETE CASCADE);
drop table salary;
insert into salary values('1235',20000);
insert into salary values(50000,'125');
select * from salary;
select * from emp2;
delete from emp2 where acc='1235';
delete from salary where acc='1235';
select * from salary;
select * from emp2;
commit;
use new;
select * from employee;
alter table employee rename column name to e_name;
select * from employee;
drop table salary;
drop table emp2;

create table emp2(name varchar(20) not null,acc varchar(30) not null ,primary key(acc));
insert into emp2 values('kamal','124');
insert into emp2 values('kumar','1245');
insert into emp2 values('kuru','1246');
insert into emp2 values('kaean','1247');
select * from emp2;
create table salary(acc varchar(30) not null,january varchar(30) not null,primary key(acc), constraint fk_amount  foreign key(acc)
references emp2(acc) ON DELETE SET NULL);
insert into salary values('124','20000');
insert into salary values('1245','30000');
insert into salary values('1246','40000');
insert into salary values('1247','50000');
select * from salary;
delete from salary where january='20000';
select * from salary;
select * from emp2;
delete from salary where acc=1246;
delete from salary where acc=1247;
delete from salary where acc=1245;
select * from emp2;
drop table salary;
drop table emp2;

create table emp2(name varchar(20) not null,acc varchar(30) not null ,primary key(acc));
insert into emp2 values('kamal','124');
insert into emp2 values('kumar','1245');
insert into emp2 values('kuru','1246');
create table salary(acc varchar(30) not null,january varchar(30) not null,primary key(acc),
constraint FK_ammount  foreign key(acc)
references emp2(acc) ON DELETE CASCADE);
insert into salary values('124','20000');
insert into salary values('1245','30000');
insert into salary values('1246','40000');
delete from salary where acc='1246';
select * from emp2;

use new;
DELIMITER $$
CREATE PROCEDURE EMP()
BEGIN
declare total int default 0;
select count(e_name) into total from employee;
select total;
END $$
DELIMITER ;

call emp() ;
drop procedure emp;
show procedure status;

select * from employee;

DELIMITER $$
CREATE PROCEDURE EMP(
IN jdsc varchar(20),
OUT total int
)
BEGIN
/* declare total int default 0;*/
select count(e_name) into total
 from employee
 where e_name=jdsc;
/* select total;*/
END $$
DELIMITER ;

SET @total=10;
call EMP('mounika',@total);
select @total; 
call EMP('kuru',@total) ; 
select @total;


drop procedure emp;


drop procedure emp;

DELIMITER $$
CREATE PROCEDURE IncrCounter(
  INOUT counter int,
  IN incr int
)
BEGIN
  SET counter = counter + incr;
END $$
DELIMITER $$

SET @counter=5;
CALL IncrCounter(@counter,2);
select @counter;
CALL IncrCounter(@counter,3);
select @counter;
